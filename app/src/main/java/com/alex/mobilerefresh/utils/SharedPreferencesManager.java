package com.alex.mobilerefresh.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.alex.mobilerefresh.MyApplication;


/**
 * single point access to the shared preferences file. This class uses the
 * default Shared Preferences and stores only authentication and seller's
 * related dataSharedPreferencesManager.java
 * 
 * @author joaojeronimo
 * 
 */
public class SharedPreferencesManager {

	private static SharedPreferences sharedPrefs;

	//this is a singleton class
	private SharedPreferencesManager(){}
	
	public static void init() {

		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());

		//SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		//Map<String, ?> sharPref = sharedPrefs.getAll();
	}
	

	public static void save(String key, String content) {
		assert key!=null;
		
		if (sharedPrefs != null && key != null) {

			sharedPrefs.edit().putString(key, content).commit();
		}
	}

	public static void save(String key, int content) {
		if (key == null) {

		}
		if (sharedPrefs != null && key != null) {

			sharedPrefs.edit().putInt(key, content).commit();
		}
	}

	public static void save(String key, boolean content) {
		if (key == null) {

		}
		if (sharedPrefs != null && key != null) {

			sharedPrefs.edit().putBoolean(key, content).commit();
		}
	}

	
	/**
	 * Get the requested value from shared preferences.
	 * If the key does not yet exist, it will be created with defaultValue 
	 * @param key - the key for the requested entry
	 * @param defaultValue - the value to create an entry, if non-existant
	 * @return the value for the requested key, or defaultValue, if the key does not yet exist
	 */
	public static String getString(String key, String defaultValue) {
		
		String value = sharedPrefs.getString(key, defaultValue);
		
		if (value == defaultValue){
			save(key,defaultValue);
			return defaultValue;
		}
		return value;
	}

	
	/**
	 * Get the requested value from shared preferences.
	 * If the key does not yet exist, it will be created with defaultValue 
	 * @param key - the key for the requested entry
	 * @param defaultValue - the value to create an entry, if non-existant
	 * @return the value for the requested key, or defaultValue, if the key does not yet exist
	 */
	public static int getInt(String key, int defaultValue) {

		int value = sharedPrefs.getInt(key, defaultValue);
		
		if (value == defaultValue){
			save(key,defaultValue);
			return defaultValue;
		}
		return value;
	}

	
	/**
	 * Get the requested value from shared preferences.
	 * If the key does not yet exist, it will be created with defaultValue 
	 * @param key - the key for the requested entry
	 * @param defaultValue - the value to create an entry, if non-existant
	 * @return the value for the requested key, or defaultValue, if the key does not yet exist
	 */
	public static boolean getBoolean(String key, boolean defaultValue) {

		boolean value = sharedPrefs.getBoolean(key, false);
		
		if (!value){
			save(key,defaultValue);
			return defaultValue;
		}
		return value;
	}

	
	
	private static String getDefaultSharedPreferencesName(Context context) {
		return context.getPackageName() + "_preferences";
	}
	
	public static boolean isInit(){
		return sharedPrefs != null;
	}

	

}
