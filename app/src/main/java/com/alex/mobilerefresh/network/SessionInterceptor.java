package com.alex.mobilerefresh.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by nb19875 on 19/06/2017.
 */

public class SessionInterceptor implements Interceptor{
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request =  original.newBuilder()
                .addHeader("Client-Time", String.valueOf(System.currentTimeMillis())) // legacy headers
                .addHeader("Client-Location", "Porto")
                .addHeader("Client-Profile", "")
                .addHeader("Client-Session","")
                .addHeader("Client-Username","test")
                .header("Accept", "application/json")
                .method(original.method(), original.body())
                .build();

        Response response =  chain.proceed(request);

        return response;
    }
}
